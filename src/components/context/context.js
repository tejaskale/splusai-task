import { createSlice , configureStore } from "@reduxjs/toolkit"

import data from "../data/data.json";

  const initialState = {
    users: [...data],
  }

export let allUsers = createSlice({name:"usersList",initialState , 
reducers: {
    getallUsers:(state,action)=> {
        return {...state,users:[...action.payload]}
    },
    deleteUser:(state,action)=>{
        return{users:[action.payload]}
    },
    addUser:(state,action)=>{
        return{users:[...state.users,action.payload]}
    },
    editUser:(state,action)=>{
        return{users:[action.payload]}
    }
}
}) 


export const { getallUsers , deleteUser , addUser , editUser } = allUsers.actions;
export default allUsers.reducer;
