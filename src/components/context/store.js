import { configureStore } from '@reduxjs/toolkit'
import allUsers from './context'

export const store = configureStore({
  reducer: {
    counter: allUsers
  }
})