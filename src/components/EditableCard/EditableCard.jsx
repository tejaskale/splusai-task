import React, { useEffect, useState } from 'react'
import "./editableCatd.css"
import { useDispatch, useSelector } from 'react-redux'
import { getallUsers , deleteUser , addUser , editUser } from "../context/context"

function EditableCard(props) {

let dispatch = useDispatch()
let allUsers=useSelector(state=>state.counter.users)
  function handleSave(){
    let obj = {}
    obj["id"] = props.edtUsrIndx
    obj["name"] = props.name;
    obj["address"] = props.address;
    obj["email"] = props.email;
    obj["mobile "] =props.mobile;
    obj["gender"] = props.gender;
    obj["city"] = props.city;
    let indx=allUsers.indexOf(props.editUser)
    props.setEditUser(obj)
    console.log(obj);
    if (indx !== -1) {
      let updatedData = [...allUsers] ;
      updatedData[indx] = obj ;
      dispatch(getallUsers(updatedData)) ;
  }
    props.setIsCardVisible(!props.isCardVisible)
    alert("Edited the item Successfully !!");

  }



  

  return (
    <div>
       {props.isCardVisible && (
        <div className="popup-container">
        <div className="popup-card">

            <h1>
                User Details
            </h1>
          
          <div className='inp-container' >
          <label htmlFor="name">Name</label>
          <input type="text"  onChange={(e)=>{props.setname(e.target.value)}} value={props.name} />
          </div>

        <div className='inp-container' >
        <label htmlFor="">address</label>
        <textarea type="text" className='address-box'  onChange={(e)=>{props.setAddress(e.target.value)}} value={props.address} />
        </div>

        <div className='inp-container'>
        <label htmlFor="">email</label>
        <input type="text"  onChange={(e)=>{props.setMail(e.target.value)}} value={props.email} />
        </div>
        
        <div className='inp-container'>
        <label htmlFor="mobile">mobile</label>
        <input type="text" id='mobile' onChange={(e)=>{props.setMobile(e.target.value)}} value={props.mobile} />
        </div>

          <div className='gender-container' >
            <p>Gender</p>
            <div>
            
            <label htmlFor="male">Male</label>
            <input type="radio" name='gender' onChange={(e)=>{props.setGender(e.target.value)}} value="male" id='male' />
           

            <label htmlFor="female">Female</label>
            <input type="radio" name='gender' value="female" onChange={(e)=>{props.setGender(e.target.value)}} id='female' />
            
            </div>

          </div>
          
          <div className='inp-container'>
          <label htmlFor="">City</label>
          <input type="text"  onChange={(e)=>{props.setCity(e.target.value)}} value={props.city} />
          </div>

          <div>
          <button onClick={handleSave}>Save</button>
          <button onClick={()=>{props.setIsCardVisible(!props.isCardVisible)}} >Close</button>
          </div>
        </div>
      </div>     
       )}
    </div>
  )
}

export default EditableCard;