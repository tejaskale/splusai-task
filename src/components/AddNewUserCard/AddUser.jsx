import React, { useEffect, useState } from 'react'
import "../EditableCard/editableCatd.css"

function AddUser(props) {







  

  return (
    <div>
       {props.isAddnewCardVisible && (
        <div className="popup-container">
        <div className="popup-card">

            <h1>
                User Details
            </h1>
          
          <div className='inp-container' >
          <label htmlFor="name">Name</label>
          <input type="text"  onChange={(e)=>{props.setname(e.target.value)}}  />
          </div>

        <div className='inp-container' >
        <label htmlFor="">address</label>
        <textarea type="text" className='address-box'  onChange={(e)=>{props.setAddress(e.target.value)}}  />
        </div>

        <div className='inp-container'>
        <label htmlFor="">email</label>
        <input type="text"  onChange={(e)=>{props.setMail(e.target.value)}}  />
        </div>
        
        <div className='inp-container'>
        <label htmlFor="mobile">mobile</label>
        <input type="number" id='mobile' onChange={(e)=>{props.setMobile(e.target.value);}}  />
        </div>

          <div className='gender-container' >
            <p>Gender</p>
            <div>
            
            <label htmlFor="male">Male</label>
            <input type="radio" name='gender' onChange={(e)=>{props.setGender(e.target.value)}} value="male" id='male' />
           

            <label htmlFor="female">Female</label>
            <input type="radio" name='gender' value="female" onChange={(e)=>{props.setGender(e.target.value)}} id='female' />
            
            </div>

          </div>
          
          <div className='inp-container'>
          <label htmlFor="">City</label>
          <input type="text"  onChange={(e)=>{props.setCity(e.target.value)}}  />
          </div>

          <div>
          <button onClick={props.handleClickOnSave}>Save</button>
          <button onClick={()=>{props.setAddNewCardVisible(!props.isAddnewCardVisible)}} >Close</button>
          </div>
        </div>
      </div>     
       )}
    </div>
  )
}

export default AddUser;