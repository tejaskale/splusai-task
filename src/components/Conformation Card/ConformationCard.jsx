import React, { Fragment, useEffect } from "react";
import "./CoformationCard-styles.css";
import { useDispatch, useSelector } from "react-redux";
import { allUsers, deleteUser, getallUsers } from "../context/context";

function ConformationCard(props) {
  let users = useSelector(state=>state.counter.users)
  let dispatch = useDispatch()

 


  const handleConformDelete = () => {
    let filteedData = users.filter((item, index) => {
      return props.deleteIndex !== item.id;
    });
    // props.setUsers(filteedData);
    dispatch(getallUsers([...filteedData]))
    props.setReadyToDelete(!props.readyToDelete);
    alert("Deleted the item Successfully !!");
  };
  return (
    <Fragment>
      {props.readyToDelete ? (
        <div className="popup-container">
          <div className="popup-card">
            <h1>Are you Sure You Want to delete the field ?</h1>
            <div>
              <button
                id="yesBtn"
                onClick={() => {
                  props.setReadyToDelete(!props.readyToDelete);
                  handleConformDelete();
                }}
              >
                Yes
              </button>
              <button
                id="closeBtn"
                onClick={() => {
                  props.setReadyToDelete(!props.readyToDelete);
                }}
              >
                Close
              </button>
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </Fragment>
  );
}

export default ConformationCard;
