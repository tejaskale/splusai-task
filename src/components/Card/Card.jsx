import React, { useState , useEffect } from "react";
import { Fragment } from "react";
import "./card_styles.css";
import { BsFillPeopleFill } from "react-icons/bs";
import { IoMdPersonAdd } from "react-icons/io";
import {
  AiFillDelete,
  AiFillEdit,
  AiOutlineArrowUp,
  AiOutlineArrowDown,
} from "react-icons/ai";
import data from "../data/data.json";
import EditableCard from "../EditableCard/EditableCard";
import AddUser from "../AddNewUserCard/AddUser";
import ConformationCard from "../Conformation Card/ConformationCard";
import { useDispatch , useSelector } from "react-redux";
import { getallUsers } from "../context/context";

function Card() {
  // let alldata = getallUsers;

  // const [users, setUsers] = useState([...alldata]);
  const [isCardVisible, setIsCardVisible] = useState(false);
  const [editUser, setEditUser] = useState();
  const [edtUsrIndx, setEdtUsrIndx] = useState("");
  const [deleteIndex, setDeleteIndex] = useState();

  let dispatch = useDispatch()
  let allUsers = useSelector(state=>state.counter.users)
  // let cardVisible = useSelector(state=>state.isCardVisible)
  // let editUser= useSelector(state=>state.editUser)
  //  let edtUsrIndx=useSelector(state=>state.edtUsrIndx)
  //  let deleteIndex=useSelector(state=>state.deleteIndex)
  // let playersNames = useSelector(state=>state.playersNames)
  //     let selectedPlayer = useSelector(state=>state.selectedPlayerName)


  const [isAddnewCardVisible, setAddNewCardVisible] = useState(false);
  const [isReverse, setIsRev] = useState(false);
  const [readyToDelete, setReadyToDelete] = useState(false);
  let [issearchOn, setIsSearchOn] = useState(false);
  const[isUserValid,setIsUserValid] = useState(false)

  const [currentPage, setCurrentPage] = useState(1);
  const [usersPerPage, setUsersPerpage] = useState(5);
  const indexOfLastUser = currentPage * usersPerPage;
  const indexOfFirstUser = indexOfLastUser - usersPerPage;
  let currentUsers = allUsers.slice(indexOfFirstUser, indexOfLastUser);





  const handleSorting = () => {
    setIsRev(!isReverse);
  };

  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const changeUsersPerPage = (e) => {
    setUsersPerpage(e.target.value);
  };

  const [name, setname] = useState("");
  const [address, setAddress] = useState("");
  const [email, setMail] = useState("");
  const [mobile, setMobile] = useState("");
  const [city, setCity] = useState("");
  const [gender, setGender] = useState("");

  function handleAddUser() {
    setAddNewCardVisible(!isAddnewCardVisible);
  }

  const handleClickOnSave = () => {
    let obj = {};
    obj["id"] = allUsers.length + 1;
    obj["name"] = name;
    obj["address"] = address;
    obj["email"] = email;
    obj["mobile "] = mobile;
    obj["gender"] = gender;
    obj["city"] = city;

    
    function validateName(){
      let name= obj.name ;
      if(name.length==0){
        alert("Invalid name")
      return  false;
      }
      if(name.match( /^[a-zA-Z]+ [a-zA-Z]+$/)){
        setIsUserValid(true)
          return true;
      }
      else{
          alert("Invalid name")
      }
      }
  validateName()

  function validatePhone(){
    var phone=obj["mobile "];
        if(phone.match(/^[0-9]{10}$/)){
            setIsUserValid(true)
        }
        else{
          setIsUserValid(false)
        }
}
validatePhone()


function validateEmail(){
  var email=obj.email;
  if(email.match(/^[A-Za-z\._\-[0-9]+[@]+[A-Za-z]+[\.]+[a-z]{2,4}$/)){
      setIsUserValid(true)
  }
  else{
    setIsUserValid(false)
  }

}
validateEmail()

if(isUserValid){
  setEditUser(obj);

  let updateddata = [...allUsers,obj]
  dispatch(getallUsers(updateddata))
  setAddNewCardVisible(!isAddnewCardVisible)
}







  };

  function handleDelete(i) {
    // let filteedData = users.filter((item,index)=>{
    //   return(
    //     i!==item.id
    //   )
    // })
    // setUsers(filteedData);
    setDeleteIndex(i);
    setReadyToDelete(!readyToDelete);
  }

  function handleEdit(editUser, i) {
    setIsCardVisible(true);
    setEdtUsrIndx(editUser.id);
    let dataTobeFiltered = [...allUsers];
    let editableData = dataTobeFiltered.filter((item, index) => {
      return editUser.id === item.id;
    });

    setEditUser(editableData[0]);
    setname(editUser.name);
    setAddress(editUser.address);
    setMail(editUser.email);
    setMobile(editUser.mobile);
    setCity(editUser.city);
    setGender(editUser.gender);

  }
  function handleSearch(e) {
    setIsSearchOn(true);
    let filtereddata = allUsers.filter((item, index) => {
      return (
        item.name.toLowerCase().includes(e.target.value.toLowerCase()) ||
        item.address.toLowerCase().includes(e.target.value.toLowerCase()) ||
        item.email.toLowerCase().includes(e.target.value.toLowerCase()) ||
        item.city.toLowerCase().includes(e.target.value.toLowerCase()) ||
        item["mobile "].toLowerCase().includes(e.target.value.toLowerCase())
      );
    }) ;
    // setUsers(filtereddata);
    dispatch(getallUsers(filtereddata));
  }

  return (
    <Fragment>
      <div className="card">
        <div className="header">
          <div>
            <BsFillPeopleFill className="logo-people" />
            <h1>User Details</h1>
          </div>
          <div>
            <input
              type="text"
              placeholder="Search here....."
              onChange={(e) => {
                handleSearch(e);
              }}
              className="search-box"
            />
            <IoMdPersonAdd
              className="logo-add-person"
              onClick={handleAddUser}
            />
          </div>
        </div>
        <div>
          <table>
            <thead>
              <tr>
                <th>
                  id{" "}
                  {isReverse ? (
                    <AiOutlineArrowDown
                      onClick={() => {
                        handleSorting();
                      }}
                    />
                  ) : (
                    <AiOutlineArrowUp
                      onClick={() => {
                        handleSorting();
                      }}
                    />
                  )}{" "}
                </th>
                <th>Name</th>
                <th>address</th>
                <th>email</th>
                <th>mobile</th>
                <th>gender</th>
                <th>city</th>
                <th>action</th>
              </tr>
            </thead>
            <tbody>
              {isReverse
                ? currentUsers.reverse().map((user, i) => {
                    return (
                      <tr key={i}>
                        <td>{user.id}</td>
                        <td>{user.name}</td>
                        <td>{user.address}</td>
                        <td>{user.email}</td>
                        <td>{user["mobile "]}</td>
                        <td>{user.gender}</td>
                        <td>{user.city}</td>
                        <td>
                          <AiFillEdit
                            className="action-logo"
                            onClick={() => {
                              handleEdit(user, i);
                            }}
                          />
                          <AiFillDelete
                            className="action-logo"
                            onClick={() => {
                              handleDelete(user.id);
                            }}
                          />
                        </td>
                      </tr>
                    );
                  })
                : currentUsers.map((user, i) => {
                    return (
                      <tr key={i}>
                        <td>{user.id}</td>
                        <td>{user.name}</td>
                        <td>{user.address}</td>
                        <td>{user.email}</td>
                        <td>{user["mobile "]}</td>
                        <td>{user.gender}</td>
                        <td>{user.city}</td>
                        <td>
                          <AiFillEdit
                            className="action-logo"
                            onClick={() => {
                              handleEdit(user, i);
                            }}
                          />
                          <AiFillDelete
                            className="action-logo"
                            onClick={() => {
                              handleDelete(user.id);
                            }}
                          />
                        </td>
                      </tr>
                    );
                  })}
            </tbody>
          </table>
        </div>
        <div className="pagination">
          <p>Rows Per Page :- </p>
          <select
            name="items-per-page"
            onChange={(e) => {
              changeUsersPerPage(e);
            }}
          >
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="15">15</option>
          </select>

          <div>
            {/* {issearchOn ? (
              " "
            ) : (
              <p>
                {currentUsers[0].id} {" - "}
                {currentUsers[currentUsers.length - 1].id}
              </p>
            )} */}
          </div>

          <button
            onClick={() => paginate(currentPage - 1)}
            disabled={currentPage === 1}
          >
            {"<"}
          </button>
          {/* {Array.from({ length: Math.ceil(users.length / usersPerPage) }, (item, index) => (
            <button
              key={index}
              onClick={() => paginate(index + 1)}
              className={currentPage === index + 1 ? "active" : ""}
            >
              {"<"}
            </button>
          ))} */}
          <button
            onClick={() => paginate(currentPage + 1)}
            disabled={currentPage === Math.ceil(allUsers.length / usersPerPage)}
          >
            {">"}
          </button>
        </div>
      </div>
      <EditableCard
        editUser={editUser}
        edtUsrIndx={edtUsrIndx}
        setEdtUsrIndx={setEdtUsrIndx}
        // users={users}
        // // setUsers={setUsers}
        setEditUser={setEditUser}
        isCardVisible={isCardVisible}
        setIsCardVisible={setIsCardVisible}
        name={name}
        setname={setname}
        address={address}
        setAddress={setAddress}
        email={email}
        setMail={setMail}
        gender={gender}
        setGender={setGender}
        mobile={mobile}
        setMobile={setMobile}
        city={city}
        setCity={setCity}
      />
      <AddUser
        name={name}
        setname={setname}
        handleAddUser={handleAddUser}
        handleClickOnSave={handleClickOnSave}
        isAddnewCardVisible={isAddnewCardVisible}
        setAddNewCardVisible={setAddNewCardVisible}
        address={address}
        setAddress={setAddress}
        email={email}
        setMail={setMail}
        gender={gender}
        setGender={setGender}
        mobile={mobile}
        setMobile={setMobile}
        city={city}
        setCity={setCity}
      />

      <ConformationCard
        // users={users}
        // setUsers={setUsers}
        readyToDelete={readyToDelete}
        setReadyToDelete={setReadyToDelete}
        deleteIndex={deleteIndex}
        setDeleteIndex={setDeleteIndex}
      />
    </Fragment>
  );
}

export default Card;
