import logo from './logo.svg';
import './App.css';
import Card from './components/Card/Card';
import EditableCard from './components/EditableCard/EditableCard';
import ConformationCard from './components/Conformation Card/ConformationCard';

function App() {
  return (
    <div className="App">
      <Card/>
      {/* <ConformationCard/> */}
    </div>
  );
}

export default App;
